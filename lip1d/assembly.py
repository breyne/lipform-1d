"""mesh and all
A structure is an assembly of elements
A load is an assembly of constraints on a structure
A problem is an assembly of material, structure and load
"""

import numpy as np
#import scipy
from scipy import optimize as sciopt

class Structure:#pylint: disable=too-many-instance-attributes
    """Hold geometry
    Limitations
    -----------
    .. This is 1D bar only
    .. The only type of tangent modulus we have is linear elastic
       (we build K using E)
    """
    def __init__(self, **kwargs):
        r"""Define a bar of unit length `x\in[0,1]`
        kwargs: dict
            material: lip.Material
            area: float
            length: float
            nb_nodes: int
        """
        # Default values
        kwargs.setdefault("material", None)
        kwargs.setdefault("length", 1)
        kwargs.setdefault("nb_nodes", 20)
        kwargs.setdefault("area", 1.)

        # Sanity checks
        if kwargs["material"] is None:
            raise Exception("Material needed to instantiate a structure")

        # Set values
        nb_nodes = kwargs["nb_nodes"]
        self.material = kwargs["material"]
        self.nodes = np.arange(0.,nb_nodes)/(nb_nodes-1)*kwargs["length"]
        self.elems = np.array([range(0,len(self.nodes)-1),
                               range(1,len(self.nodes))
                               ]).T
        self.area = kwargs["area"]
        self.diff_x = kwargs["length"]/(nb_nodes-1)

        # Quick parameters
        self.dim = 1
        self.nb_nodes = nb_nodes
        self.nb_elems = nb_nodes-1
        self.nb_integration_points = nb_nodes-1
        self.len_strain = nb_nodes-1
        self.len_u = nb_nodes#*self.dim
        self.len_alpha = (nb_nodes-1)*sum(self.material.variables_types.values())
        self.len_u_alpha = self.len_u + self.len_alpha

    def strain(self, disp):
        """Return the strain measure given a NODAL displacement field"""
        # assert u.shape == node.shape somewhere globally
        #return np.gradient(disp, self.nodes)
        return (disp[1:]-disp[:-1]) / self.diff_x

    def dstrain_du(self) :
        """compute gradient of strain with regard to u (B matrix)"""
        nelem = len(self.elems)
        nvert = len(self.nodes)
        return  -1./self.diff_x*np.eye(nelem,nvert)\
                +1./self.diff_x*np.eye(nelem,nvert,1)

    def integrate(self, field):
        """Integrate a field given at gauss points"""
        return np.sum(field) * self.area * self.diff_x

    ######################################################################
    ##
    ######################################################################

    def variables_bounds(self, **kwargs):
        """Return a scipy.optimize.Bounds instance
        corresponding to u_alpha boundaries
        """

        kwargs.setdefault("u_imp", None)
        if kwargs["u_imp"] is None:
            raise Exception("The key `u_imp`:float is needed.")

        # Bounds instance for U

        ub_u = (np.zeros(self.len_u)+1.) * np.inf
        ub_u[[0,-1]] = [0., kwargs["u_imp"]]
        lb_u = ub_u*-1
        lb_u[-1] = kwargs["u_imp"]

        # Complete with the alphas
        kwargs.setdefault("u_alpha_n", None)
        if isinstance(kwargs["u_alpha_n"], np.ndarray):
            vars_n = self.global_to_local_variables(kwargs["u_alpha_n"])
            lb_alpha, ub_alpha = self.material.variables_bounds(**vars_n)
        else:
            ub_alpha = np.ones(self.len_alpha)*np.inf
            lb_alpha = ub_alpha*(-1)

        return sciopt.Bounds(#lb <= x <= ub
                np.hstack([lb_u, lb_alpha]),# lower bounds
                np.hstack([ub_u, ub_alpha]),# upper bounds
                )

    def equilibrium_constraint(self, string_option='None'):
        """Return a scipy.optimize.LinearConstraint instance
        corresponding to
            K.du = f     <=> A.dot(x) = b
        In practice, we build it with
            lb <= A.dot(x) <= ub
        """
        mat = self.material
        # Assemble the big B (dstrain_du)
        b_tot = self.dstrain_du()
        # Assemble the big D (local tangent modulus)
        d_tot = np.diag(np.ones(self.len_strain)) * mat.parameters["E"]
        #D = scipy.sparse.diags(np.ones(len(self.elems)))*mat.parameters["E"]

        k_tot = self.diff_x * b_tot.T.dot(d_tot.dot(b_tot))

        bnds_sup_f = np.zeros(self.len_u)
        bnds_sup_f[[0,-1]] = np.inf
        bnds_inf_f = bnds_sup_f * -1.



        # fill with zeros for alpha
        total_linop = np.zeros([self.len_u_alpha]*2)
        total_linop[:self.len_u, :self.len_u] = k_tot*1
        bnds_inf_all = np.hstack([bnds_inf_f, np.zeros(self.len_alpha)])
        bnds_sup_all = np.hstack([bnds_sup_f, np.zeros(self.len_alpha)])

        # Deal with the plastic case
        if 'elastic' not in string_option.lower() \
            and 'plastic_strain' in mat.variables_types.keys():
            # Then there is a linear operator for the plastic strains as well
            # {ε*}{σ} = {u*}[Bt][D]{ε-εp}
            #         = {u*}[Bt][D]( [B]{u} - {εp})
            #         = {u*}[Bt][D][B]{u} - {u*}[Bt][D]{εp}
            k_epsp = -b_tot.T.dot(d_tot)
            cols = slice(self.len_u, int(self.len_u+self.len_alpha/2))
            total_linop[:self.len_u, cols] = k_epsp*1

        return sciopt.LinearConstraint(
                total_linop,# A,
                bnds_inf_all,# lb,
                bnds_sup_all,# ub,
                #keep_feasible=False
                )

    def potential_hardening(self, u_alpha):
        """integrate the local constitutive potential
        Attributes
        ----------
        u_alpha: 1darray
            concatenation of
            nodal displacements u and
            internal variables alpha
        """
        # Make sure the shape makes sense
        assert len(u_alpha) == self.len_u_alpha

        # Turn the vector into a dict with split values
        # and appropriate names

        # Split u from the rest and Initialize variables dict
        variables = self.global_to_local_variables(u_alpha)

        pot_local = self.material.potential_hardening(**variables)
        return self.integrate(pot_local)

    def global_to_local_variables(self, u_alpha):
        """Turn the vector into a dict with split values
        and appropriate names
        """
        # Split u from the rest and Initialize variables dict
        variables = {'strain':self.strain(u_alpha[:self.len_u])}

        # Stop there if there is nothing to be done
        if self.len_alpha == 0:
            return variables

        # Split every alpha  from the rest
        row=self.len_u
        for vname, vvalue in self.material.variables_types.items():
            len_alpha = vvalue * self.nb_integration_points
            variables[vname] = u_alpha[row:row+len_alpha]
            row += len_alpha

        return variables
