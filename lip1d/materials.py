"""materials library
"""
import abc
import scipy
import numpy as np

class Material(abc.ABC):
    """ A material base class (abstract)."""
    expected_params = list()
    variables_types = dict()
    def __init__(self, **kwargs):
        """Instance
        var_types: numpy.dtype
            variables in addition to strain.
        """
        for par in self.expected_params:
            if par not in kwargs.keys():
                raise Exception('Missing parameter: '+par+'!')
        self.parameters = kwargs

    @abc.abstractmethod
    def potential_hardening(self, **kwargs):
        """The good potential: convex and
        """

    def potential_softening(self, **kwargs):
        """The convex that softens"""

    def variables_bounds(self, **kwargs):
        """Returns scipy.optimize.Bounds instance"""
        return None

    def variables_constraints(self, **kwargs):
        """Returns scipy.optimize.(NON)LinearConstraints instance"""
        return None

################################################################################
## -------------------------------------------------------------------------- ##
################################################################################

class Elastic(Material):
    """Linear Elastic material SIG = E EPS"""

    expected_params = ('E',)
    variables_types = dict()

    def potential_hardening(self, **kwargs):
        strain = kwargs["strain"]
        return strain**2*self.parameters["E"]/2

    def variables_bounds(self, **kwargs):
        return None

    def variables_constraints(self, **kwargs):
        return None

################################################################################
## -------------------------------------------------------------------------- ##
################################################################################

class ElastoPlastic(Material):
    """Linear Elastic material SIG = E (EPS-EPSP)
    with yield function
    """
    expected_params = ('E', 'sigma_y', 'k',)
    variables_types = {
            "plastic_strain":1,
            "accumulated_plastic_strain":1,
            }

    ##----------------------------------------------------------------------- ##
    ## Requirements --------------------------------------------------------- ##
    ##----------------------------------------------------------------------- ##

    def potential_hardening(self, **kwargs):
        eps = kwargs["strain"]
        epsp = kwargs["plastic_strain"]
        aps = kwargs["accumulated_plastic_strain"]
        return self.parameters["E"]/2. * (eps-epsp)**2 \
               + self.parameters['sigma_y']*(aps+self.parameters['k']*aps**2/2.)

    def variables_bounds(self, **vars_n):
        """Enforce:
        Deviatoric plastic strains (nothing in 1D)
        Positive APS increment.
        """
        size=len(vars_n['strain'])
        l_b = np.hstack([
            np.array([-np.inf]*size),#epsp
            vars_n["accumulated_plastic_strain"],#aps
            ])
        u_b = np.hstack([
            np.array([+np.inf]*size),#epsp
            np.array([+np.inf]*size),#aps
            ])
        return l_b, u_b

    def variables_constraints(self, **variables):
        """probably f leq0 here
        """
        return variables.keys()
        #fun = self.yield_surf(**variables)
        #return scipy.optimize.NonlinearConstraint(
        #    fun,
        #    lb,
        #    ub,
        #    #jac='2-point',
        #    #hess=<scipy.optimize._hessian_update_strategy.BFGS object>,
        #    #keep_feasible=False,
        #    #finite_diff_rel_step=None,
        #    #finite_diff_jac_sparsity=None,
        #    )

    ##----------------------------------------------------------------------- ##
    ## The rest ------------------------------------------------------------- ##
    ##----------------------------------------------------------------------- ##

    def yield_surf(self, **variables):
        """the yield function for setting constraints"""
        sig = self.potential_hardening_derivative(
                "strain", **variables
                )
        hard = self.potential_hardening_derivative(
                "accumulated_plastic_strain", **variables
                )
        return abs(sig)-hard

    def potential_hardening_derivative(self, varname, **kwargs):
        """Compute derivatives"""
        if varname == "strain":
            return + self.parameters["E"]*kwargs["strain"]
        if varname == "plastic_strain":
            return - self.parameters["E"]*kwargs["plastic_strain"]
        if varname == "accumulated_plastic_strain":
            aps = kwargs["accumulated_plastic_strain"]
            return self.parameters['sigma_y']*(1+self.parameters['k']*aps)
        raise Exception('Wrong `varname`')
