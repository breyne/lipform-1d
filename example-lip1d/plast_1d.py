""" Trying plasticity
"""
import numpy as np
import scipy
from context import materials as mat, assembly as ass

YOUNG_MODULUS = 1e5
YIELD_STRESS = 100
LENGTH = 10.

MX_EL = mat.Elastic(E=YOUNG_MODULUS,)
MX = mat.ElastoPlastic(E=YOUNG_MODULUS, k=1., sigma_y=YIELD_STRESS)
ST = ass.Structure(length=10., nb_nodes=4, material=MX)

YLD_DISP = YIELD_STRESS / YOUNG_MODULUS * LENGTH

##############################################################################
## Build and solve a scipy minimization problem --------------------------- ##
##############################################################################

u_alpha_n = np.zeros(ST.len_u_alpha)# should be the elastic solution
UL = .1

## Build The NLConstraint for yieldsurf
def yield_function(u_alpha):
    """The yld function"""
    variables = ST.global_to_local_variables(u_alpha)
    return MX.yield_surf(**variables)

yld_fun_constraint = scipy.optimize.NonlinearConstraint(
        yield_function,
        -np.inf,#lb
        0.,#ub,
        #jac='2-point',
        #hess=<scipy.optimize._hessian_update_strategy.BFGS object>,
        #keep_feasible=False,
        #finite_diff_rel_step=None,
        #finite_diff_jac_sparsity=None,
        )

## Now minimize it while imposing U0 and UL and equil
res = scipy.optimize.minimize(
        ST.potential_hardening,# function to minimize
        u_alpha_n,# x0:
        method='trust-constr',# constrained trust region
        bounds=ST.variables_bounds(u_imp=UL,),
        constraints=(
            ST.equilibrium_constraint('no option'),
            yld_fun_constraint,
            ),
)
