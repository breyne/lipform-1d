""" Try a simple variational form
"""
import numpy as np
#import scipy
from scipy import optimize as sciopt

from context import materials as mat, assembly as ass

MX = mat.Elastic(E=1e8, nu=.33)
ST = ass.Structure(length=10., nb_nodes=4, material=MX)


##############################################################################
## Build and solve a scipy minimization problem --------------------------- ##
##############################################################################

U0 = np.zeros(ST.len_u_alpha)
UL = 1.

## Now minimize it while imposing U0 and UL and equil
res = sciopt.minimize(
        ST.potential_hardening,# fun:
        U0,# x0:
        method='trust-constr',# method=None, constrained trust region
        bounds=ST.variables_bounds(u_imp=UL),# bounds=None,
        constraints=ST.equilibrium_constraint(),# constraints=(),
)
