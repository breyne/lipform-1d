"""Context file for path-agnostic inclusion."""
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

#pylint: disable=wrong-import-position
#pylint: disable=unused-import
#pylint: disable=import-error
from lip1d import materials
from lip1d import assembly
from lip1d import solver
