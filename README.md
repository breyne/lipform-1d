# Lipform 1d

A Lip-field implementation after the paper and concept of [Moës and Chevaugeon (2021)](http://dx.doi.org/10.5802/crmeca.91).

# Todos:

Immediately

- [ ] Refactor names: `global_to_local_variables`, `loc_vars` instead of `v ariables`
- [ ] Equilibrium must take the plastic strain into account
- [ ] Make the elastic solution the initial guess
- [ ] `test/plast` solve over several increments

Eventually
- [ ] Refactor docstrings
- [ ] Include logging
- [ ] Include sanity checks and exception (global only)
